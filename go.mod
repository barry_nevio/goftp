module github.com/jlaffaye/ftp

go 1.13

require (
	github.com/machinebox/progress v0.2.0 // indirect
	github.com/spf13/cast v1.3.1
	github.com/stretchr/testify v1.4.0
)
